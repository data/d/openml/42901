# OpenML dataset: caesarian-section

https://www.openml.org/d/42901

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

We choose age, delivery number, delivery time, blood pressure and heart status.
We classify delivery time to Premature, Timely and Latecomer. As like the delivery time we consider blood pressure in three statuses of Low, Normal and High moods. Heart Problem is classified as apt and inept.

@attribute 'Age' { 22,26,28,27,32,36,33,23,20,29,25,37,24,18,30,40,31,19,21,35,17,38 }
@attribute 'Delivery number' { 1,2,3,4 }
@attribute 'Delivery time' { 0,1,2 } -&gt; {0 = timely , 1 = premature , 2 = latecomer}
@attribute 'Blood of Pressure' { 2,1,0 } -&gt; {0 = low , 1 = normal , 2 = high }
@attribute 'Heart Problem' { 1,0 } -&gt; {0 = apt, 1 = inept }

@attribute Caesarian { 0,1 } -&gt; {0 = No, 1 = Yes }

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42901) of an [OpenML dataset](https://www.openml.org/d/42901). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42901/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42901/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42901/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

